﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Die> dice = new List<Die>();
            for(int i = 0; i<20; i++)
            {
                dice.Add(new Die(6));
            }

            DiceRoller diceroll = new DiceRoller();

            foreach(Die die in dice)
            {
                diceroll.InsertDie(die);
            }

            diceroll.RollAllDice();

            diceroll.GetRollingResults();

            Console.ReadKey();
        }
    }
}
